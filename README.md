# Chapter-3 Daily Task 1 

Pembagian tugas daily task pada team 2:
- [`luasPersegi.js`](./luasPersegi.js) : Nadir
- [`luasSegitiga.js`](./luasSegitiga.js) : Albi
- [`kelilingSegitiga.js`](./kelilingSegitiga.js) : Albi
- [`luasLingkaran.js`](./luasLingkaran.js) : Zidan
- [`hitungModulo.js`](./hitungModulo.js) : Bregsi
- [`penjumlahan.js`](./penjumlahan.js) : Selly

```javascript
 console.log("Nice Work Team 🤩🤩🤩");
```
