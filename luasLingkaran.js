//  function luas lingkara
function luas(r) {
  const phi = 3.14;
  L = phi * (r * r);
  return L;
}
// export function
module.exports = luas;