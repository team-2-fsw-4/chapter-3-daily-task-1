// function luas Segitiga
function luasSegitiga(alas, tinggi) {
  return (alas * tinggi) / 2;
}

// export function
module.exports = luasSegitiga;
