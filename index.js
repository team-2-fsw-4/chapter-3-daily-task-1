// install readline module = module dari node js
var readline = require('readline');

// import file sama function di dalamnya
const luasPersegi = require('./luasPersegi');
const luasSegitiga = require('./luasSegitiga');
const kelilingSegitiga = require('./kelilingSegitiga');
const luasLingkaran = require('./luasLingkaran');
const hitungModulo = require('./hitungModulo');
const hitungPenjumlahan = require('./penjumlahan');


// menggunakan readline module
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

// question 1 by nadir
console.log('=> Menghitung Luas Persegi');
rl.question('Masukkan nilai sisi persegi ', (sisi) => {
  const hasilPrintLuasPersegi = luasPersegi(sisi);
  console.log(hasilPrintLuasPersegi);
  // question 2 by albi
  console.log('\n=> Menghitung Luas Segitiga');
  rl.question('Masukkan nilai alas ', (alas) => {
    rl.question('Masukkan nilai tinggi ', (tinggi) => {
      const hasilPrintLuasSegitiga = luasSegitiga(alas, tinggi);
      console.log(hasilPrintLuasSegitiga);
      // question 3 by albi
      console.log('\n=> Menghitung Keliling Segitiga');
      rl.question('Masukkan nilai sisi A segitiga ', (sisiA) => {
        rl.question('Masukkan nilai sisi B segitiga ', (sisiB) => {
          rl.question('Masukkan nilai sisi C segitiga ', (sisiC) => {
            const hasilPrintKelilingSegitiga = kelilingSegitiga(sisiA, sisiB, sisiC);
            console.log(hasilPrintKelilingSegitiga);
            // question 4 by zidan
            console.log('\n=> Menghitung Luas Lingkaran');
            rl.question('Masukkan nilai jari-jari lingkaran ', (r) => {
              const hasilLuasLingkaran = luasLingkaran(r);
              console.log('luas(r): ' + hasilLuasLingkaran);
              // question 5 by bregsi
              console.log('\n=> Menghitung Modulo (Sisa Bagi)');
              rl.question('Masukkan angka pertama: ', (num1) => {
                rl.question('Masukkan angka kedua: ', (num2) => {
                  const hasilHitungModulo = hitungModulo(num1, num2);
                  console.log('Hasil ' + num1 + ' % ' + num2 + ' adalah ' + hasilHitungModulo);
                  // question 6 by selly
                  console.log('\n=> Menghitung Penjumlahan');
                  rl.question('Masukkan angka pertama: ', (num1) => {
                  rl.question('Masukkan angka kedua: ', (num2) => {
                    const hasilHitungPenjumlahan = hitungPenjumlahan(num1, num2);
                    console.log(hasilHitungPenjumlahan);
                  rl.close();
                  });
                 });
                });
              });
            });
          });
        });
      });
    });
  });
});
